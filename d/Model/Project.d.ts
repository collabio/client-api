import { Scene } from './Scene';
export interface Project {
    _id: number;
    name: string;
    scenes: Scene[];
}
