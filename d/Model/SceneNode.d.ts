import { Vector3 } from './Vector3';
import { Mesh } from './Mesh';
export interface SceneNode {
    _id: string;
    name: string;
    parent: SceneNode;
    children: SceneNode[];
    position: Vector3;
    mesh: Mesh;
}
