export declare class Event<T1, T2> {
    private listeners;
    constructor();
    Subscribe(listener: (...obj: (T1 | T2)[]) => void): void;
    Fire(...obj: (T1 | T2)[]): void;
}
