import { SceneNode } from './SceneNode';
export interface Scene {
    _id: number;
    name: string;
    root: SceneNode;
}
