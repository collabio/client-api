export declare class ChatMessage {
    protected text: string;
    constructor(text: string);
    Serialize(): any;
    static Deserialize(object: any): ChatMessage;
}
