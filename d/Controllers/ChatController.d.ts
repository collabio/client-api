import { Client } from 'collab.io-networking';
import { ChatMessage } from '../Model/ChatMessage';
export declare class ChatController {
    protected client: Client;
    private messages;
    private channel;
    readonly Messages: ChatMessage[];
    constructor(client: Client);
    SendMessage(text: string): void;
    OnMessage(callback: (message) => void): void;
}
