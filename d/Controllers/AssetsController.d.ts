import { ApiClient, Client, Namespace } from 'collab.io-networking';
import { Mesh } from '../Model/Mesh';
import { Event } from '../Model/Event';
export declare class AssetsController {
    protected apiClient: ApiClient;
    protected client: Client;
    protected projectId: string;
    protected channel: Namespace;
    protected list: Mesh[];
    protected onAssetAdded: Event<Mesh, null>;
    readonly List: Mesh[];
    readonly OnAssetAdded: Event<Mesh, null>;
    Upload(assetData: any): Promise<any>;
    BroadcastAddAsset(asset: Mesh): void;
    AddAsset(asset: Mesh): void;
    constructor(apiClient: ApiClient, client: Client, projectId: string);
}
