import { Client, Namespace, ApiClient } from 'collab.io-networking';
import { SceneController } from './SceneController';
import { Scene } from '../Model/Scene';
import { SceneNode } from '../Model/SceneNode';
import { Project } from '../Model/Project';
import { Event } from '../Model/Event';
export declare class EditorController {
    protected client: Client;
    protected apiClient: ApiClient;
    protected projectId: string;
    protected activeScene: SceneController;
    protected project: Project;
    protected channel: Namespace;
    protected onSceneAdded: Event<Scene, null>;
    readonly Client: Client;
    readonly ApiClient: ApiClient;
    readonly ProjectId: string;
    ActiveScene: SceneController;
    readonly Project: Project;
    readonly OnSceneAdded: Event<Scene, null>;
    constructor(client: Client, apiClient: ApiClient, projectId: string);
    LoadProject(): Promise<Project>;
    BroadcastAddScene(scene: Scene): Promise<void>;
    AddScene(scene: Scene): Promise<void>;
    GetSceneByNode(id: string): Scene;
    GetNodeById(id: string): SceneNode;
    private RecursiveNodeSearch(node, id);
}
