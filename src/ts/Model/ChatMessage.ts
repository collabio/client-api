export class ChatMessage {
    public constructor(protected text: string) {

    }

    public Serialize(): any {
        return { text: this.text };
    }

    public static Deserialize(object): ChatMessage {
        return new ChatMessage(object.text);
    }
}