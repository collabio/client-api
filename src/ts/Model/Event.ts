export class Event<T1, T2> {
    private listeners: ((...obj: (T1 | T2)[]) => void)[];

    public constructor() {
        this.listeners = [];
    }

    public Subscribe(listener: (...obj: (T1 | T2)[]) => void) {
        this.listeners.push(listener);
    }

    public Fire(...obj: (T1 | T2)[]) {
        for(let listener of this.listeners)
            listener(...obj);
    }
}