import { Client, Namespace, ApiClient } from 'collab.io-networking';

import { SceneController } from './SceneController';
import { AssetsController } from './AssetsController';

import { Scene } from '../Model/Scene';
import { SceneNode } from '../Model/SceneNode';
import { Project } from '../Model/Project';
import { Event } from '../Model/Event';

export class EditorController {

    protected activeScene: SceneController;
    protected project: Project;
    protected channel: Namespace;

    protected onSceneAdded: Event<Scene, null>;

    public get Client(): Client {
        return this.client;
    }
    public get ApiClient(): ApiClient {
        return this.apiClient;
    }

    public get ProjectId(): string {
        return this.projectId;
    }
    public get ActiveScene(): SceneController {
        return this.activeScene;
    }
    public get Project(): Project {
        return this.project;
    }
    public set ActiveScene(value: SceneController) {
        if (this.activeScene)
            this.activeScene.Deactivate();
        this.activeScene = value;
        this.activeScene.Activate();
    }

    public get OnSceneAdded(): Event<Scene, null> {
        return this.onSceneAdded;
    }

    public constructor(protected client: Client,
        protected apiClient: ApiClient,
        protected projectId: string) {
        this.onSceneAdded = new Event();
        this.channel = client.Room(projectId.toString()).Namespace('project');

        this.channel.On('addScene', data => {
            this.AddScene(data.scene);
        })
    }

    public async LoadProject(): Promise<Project> {
        if (!this.project) {
            let project = await this.apiClient.Get(`projects/${this.projectId}`)
            this.project = project;

            for (let scene of project.scenes) {
                this.onSceneAdded.Fire(scene);
            }
        }
        return this.project;
    }

    public async BroadcastAddScene(scene: Scene) {
        await this.AddScene(scene);
        this.channel.Broadcast('addScene', { scene });
    }

    public async AddScene(scene: Scene) {
        let result = await this.apiClient.Put(`projects/${this.projectId}/scenes`, scene);
        scene._id = result._id;
        this.project.scenes.push(scene);
        this.onSceneAdded.Fire(scene);

        
        //listen for scene nodes
    }
}