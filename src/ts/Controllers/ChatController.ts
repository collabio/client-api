import { Client, Namespace } from 'collab.io-networking';
import { ChatMessage } from '../Model/ChatMessage';

export class ChatController {
    private messages: ChatMessage[];
    private channel: Namespace;

    public get Messages(): ChatMessage[] {
        return this.messages;
    }

    public constructor(protected client: Client) {
        this.messages = [];
        this.channel = this.client.Room('general').Namespace('chat');
        
        this.OnMessage(message => {
            this.messages.push(message)
        });
    }

    public SendMessage(text: string) {
        let message = new ChatMessage(text);
        
        this.channel.Broadcast('message', message);
    }

    public OnMessage(callback: (message) => void) {
        this.channel.On('message', obj => callback(ChatMessage.Deserialize(obj)));
    }
}