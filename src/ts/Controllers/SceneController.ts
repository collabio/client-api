import { Client, Namespace, ApiClient } from 'collab.io-networking';
import { Event } from '../Model/Event';

import { Scene } from '../Model/Scene';
import { SceneNode } from '../Model/SceneNode';

export class SceneController {

    protected channel: Namespace;
    protected scene: Scene;
    protected sceneIsActive: boolean;

    protected onNodeAdded: Event<SceneNode, SceneNode>;
    protected onNodeRemoved: Event<SceneNode, null>;
    protected onActivation: Event<null, null>;
    protected onDeactivation: Event<null, null>;

    public get Root() {
        return this.scene.root;
    }

    public get SceneIsActive() {
        return this.sceneIsActive;
    }

    public get OnNodeAdded() {
        return this.onNodeAdded;
    }

    public get OnNodeRemoved() {
        return this.onNodeRemoved;
    }

    public get OnActivation() {
        return this.onActivation;
    }

    public get OnDeactivation() {
        return this.onDeactivation;
    }

    public constructor(protected apiClient: ApiClient,
        protected client: Client,
        protected projectId: string,
        protected sceneId: number) {
        this.channel = client.Room(projectId).Namespace('editor');
        this.onNodeAdded = new Event();
        this.onNodeRemoved = new Event();
        this.onActivation = new Event();
        this.onDeactivation = new Event();
        this.sceneIsActive = false;

        this.channel.On('addNode', data => {
            let node = this.FindNodeById(data.to);
            this.AddNode(node, data.node);
        });
        this.channel.On('removeNode', data => {
            let node = this.FindNodeById(data.id);
            this.RemoveNode(node);
        });

        this.Load();
    }

    public Activate() {
        this.onActivation.Fire();
        this.sceneIsActive = true;
    }

    public Deactivate() {
        this.onDeactivation.Fire();
        this.sceneIsActive = false;
    }

    public async Load(): Promise<Scene> {
        if (!this.scene) {
            let scene = await this.apiClient.Get(`projects/${this.projectId}/scenes/${this.sceneId}`);
            this.scene = scene;

            for (let node of scene.root.children) {
                this.AddNode(this.scene.root, node);
            }
        }
        return this.scene;
    }

    public BroadcastAddNode(to: SceneNode, node: SceneNode) {
        if (to != null) {
            this.channel.Broadcast('addNode', { to: to._id, node: node });
            this.AddNode(to, node);
        }
    }

    public BroadcastRemoveNode(node: SceneNode) {
        if (node != null) {
            this.channel.Broadcast('removeNode', { id: node._id });
            this.RemoveNode(node);
        }
    }

    public AddNode(to: SceneNode, node: SceneNode) {
        if (to != null) {
            node.parent = to;
            to.children.push(node);
            this.onNodeAdded.Fire(to, node);
        }
    }

    public RemoveNode(node: SceneNode) {
        if (node != null) {
            node.parent.children.splice(node.parent.children.indexOf(node), 1);
            node.parent = null;
            this.onNodeRemoved.Fire(node);
        }
    }

    public FindNodeById(id: string): SceneNode {
        if (id == this.scene.root._id)
            return this.scene.root;
        else
            return this.FindDescendantById(this.scene.root, id);
    }

    protected FindDescendantById(node: SceneNode, id: string) {
        if (node.children.length == 0)
            return null;

        for (let child of node.children) {
            if (child._id == id)
                return child;
        }
        for (let child of node.children) {
            let result = this.FindDescendantById(child, id);
            if (result)
                return result;
        }
        return null;
    }

    public static ConstructScene(name: string): Scene {
        return { _id: null, name: name, root: SceneController.ConstructNode('Root') };
    }

    public static ConstructNode(name: string): SceneNode {
        let guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });

        return { name: name, parent: null, children: [], _id: guid, mesh: null, position: { x: 0, y: 0, z: 0 } };
    }
}