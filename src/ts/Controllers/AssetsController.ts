import { ApiClient, Client, Namespace } from 'collab.io-networking';

import { Mesh } from '../Model/Mesh';
import { Event } from '../Model/Event';

export class AssetsController {

    protected channel: Namespace;
    protected list: Mesh[];

    protected onAssetAdded: Event<Mesh, null>;

    public get List() {
        return this.list;
    }

    public get OnAssetAdded() {
        return this.onAssetAdded;
    }

    public async Upload(assetData: any) {
        let asset = await this.apiClient.Put(`projects/${this.projectId}/assets`, assetData);
        this.BroadcastAddAsset(asset);
        return asset;
    }

    public BroadcastAddAsset(asset: Mesh) {
        this.channel.Broadcast('addAsset', asset);
        this.AddAsset(asset);
    }

    public AddAsset(asset: Mesh) {
        this.list.push(asset);
        this.onAssetAdded.Fire(asset);
    }

    public constructor(protected apiClient: ApiClient,
        protected client: Client,
        protected projectId: string) {
        this.list = [];
        this.channel = client.Room(projectId).Namespace('editor');
        this.onAssetAdded = new Event();
        this.channel.On('addAsset', asset => {
            this.AddAsset(asset);
        });

        (async () => {
            let assets = await this.apiClient.Get(`projects/${this.projectId}/assets`);
            for (let asset of assets) {
                this.AddAsset(asset);
            }
        })();
    }
}